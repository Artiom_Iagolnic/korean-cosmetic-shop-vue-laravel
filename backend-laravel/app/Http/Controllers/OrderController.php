<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class OrderController extends Controller
{
    public function store(Request $request): JsonResponse
    {
        $orderData = $request->only([
            'user_id',
            'total_amount'
        ]);

        $order = Order::create($orderData);

        foreach ($request->input('items') as $item) {
            $product = Product::find($item['product_id']);

            if ($product) {
                $orderItemData = [
                    'order_id' => $order->id,
                    'product_id' => $product->id,
                    'quantity' => $item['quantity'],
                    'price' => $item['price'],
                ];

                OrderItem::create($orderItemData);
            }
        }

        return response()->json([
            'message' => 'Order created successfully'
        ]);
    }


    public function index(): JsonResponse
    {
        $orders = Order::with('user', 'items.product')->get();

        $ordersWithUsersAndProducts = $orders->map(function ($order) {
            return [
                'id' => $order->id,
                'status' => $order->status,
                'total_amount' => $order->total_amount,
                'created_at' => $order->created_at,
                'user' => $order->user,
                'products' => $order->items->map(function ($item) {
                    return [
                        'id' => $item->product->id,
                        'name' => $item->product->name,
                        'description' => $item->product->description,
                        'price' => $item->price,
                        'quantity' => $item->quantity,
                    ];
                }),
            ];
        });

        return response()->json([
            'message' => 'success',
            'orders' => $ordersWithUsersAndProducts
        ]);
    }

}
