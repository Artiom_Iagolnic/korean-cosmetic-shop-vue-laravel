<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use PhpParser\Node\Stmt\TryCatch;

class CustomerController extends Controller
{
    public function index(): JsonResponse
    {
        try {
            $customers = Customer::all();

            return response()->json(['status' => 'success', 'customers' => $customers], 200);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'server error', 'message' => $th->getMessage()], 500);
        }

    }
    public function destroy(Customer $customer): JsonResponse
    {
        $customer = Customer::find($customer->id);
        if ($customer) {
            $customer->delete();
            return response()->json(['user' => $customer->id], 200);
        } else {
            return response()->json(['message' => 'User not found.'], 404);
        }
    }
}
