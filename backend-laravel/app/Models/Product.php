<?php

namespace App\Models;

use App\Models\OrderItem;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
        'description',
        'img_url',
        'price',
    ];


    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }
    protected static function boot()
    {
        parent::boot();

        // Generate slug before creating a new product
        static::creating(function ($product) {
            $product->slug = static::generateUniqueSlug($product->name);
        });

        // Update slug when updating a product
        static::updating(function ($product) {
            $product->slug = static::generateUniqueSlug($product->name, $product->id);
        });
    }

    protected static function generateUniqueSlug($name, $productId = null)
    {
        $baseSlug = Str::slug($name);

        // If productId is provided (for updates), exclude the current product from the check
        $query = static::where('slug', 'like', $baseSlug . '%');
        if ($productId) {
            $query->where('id', '<>', $productId);
        }

        $count = $query->count();

        // If there are duplicates, append a unique identifier
        return $count > 0 ? "{$baseSlug}-{$count}" : $baseSlug;
    }

}
