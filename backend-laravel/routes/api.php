<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CustomerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::post('/order', [OrderController::class, 'store']);
});

Route::middleware(['admin'])->group(function () {
    Route::apiResource('customers', CustomerController::class)->only(['index', 'destroy']);
    Route::apiResource('products', ProductController::class)->only(['destroy', 'update', 'store']);

});
Route::apiResource('orders', OrderController::class)->only(['index', 'destroy', 'update']);
Route::get('/products', [ProductController::class, 'index']);



