<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        foreach (range(1, 20) as $index) {
            Product::create([
                'name' => fake()->word,
                'slug' => fake()->slug,
                'description' => fake()->sentence,
                'img_url' => fake()->imageUrl,
                'price' => fake()->randomFloat(2, 10, 100),
            ]);
        }
    }
}
