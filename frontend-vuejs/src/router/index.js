// router/index.js
import { createRouter, createWebHistory } from "vue-router";
import HomePage from "../views/pages/HomePage.vue";
import AboutPage from "../views/pages/AboutPage.vue";
import ProductListingPage from "../views/pages/ProductListingPage.vue";
import ProductDetailsPage from "../views/pages/ProductDetailsPage.vue";
import E404Page from "../views/pages/E404Page.vue";
import AdminDashboardPage from "../views/admin/AdminDashboardPage.vue";
import ProductsControlPage from "../views/admin/products/ProductsControlPage.vue";
import OrdersControlPage from "../views/admin/orders/OrdersControlPage.vue";
import CustomersControlPage from "../views/admin/customers/CustomersControlPage.vue";
import RegisterComponent from "@/components/auth/RegisterComponent.vue";
import LoginComponent from "@/components/auth/LoginComponent.vue";
import ForgotPasswordComponent from "@/components/auth/ForgotPasswordComponent.vue";
import ShoppingCartPage from "@/views/pages/ShoppingCartPage.vue";
//import { useStore } from "vuex";

const routes = [
  {
    path: "/",
    name: "Home",
    component: HomePage,
  },
  {
    path: "/about",
    name: "About",
    component: AboutPage,
  },
  {
    path: "/products",
    name: "Products",
    component: ProductListingPage,
  },
  {
    path: "/products/:slug",
    name: "Product",
    component: ProductDetailsPage,
  },
  {
    path: "/cart",
    name: "Cart",
    component: ShoppingCartPage,
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: AdminDashboardPage,
    meta: {
      requiresAdmin: true,
    },
    children: [
      {
        path: "products",
        name: "ProductsControl",
        component: ProductsControlPage,
      },
      {
        path: "orders",
        name: "OrdersControl",
        component: OrdersControlPage,
      },
      {
        path: "customers",
        name: "CustomersControl",
        component: CustomersControlPage,
      },
    ],
  },
  {
    path: "/register",
    name: "Register",
    component: RegisterComponent,
  },
  {
    path: "/login",
    name: "Login",
    component: LoginComponent,
  },
  {
    path: "/forgot-password",
    name: "ForgotPassword",
    component: ForgotPasswordComponent,
  },
  {
    path: "/:any(.*)",
    component: E404Page,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});
// router.beforeEach((to, from, next) => {
//   const store = useStore();
//   const isAuthenticated = store.getters["user/isAuthenticated"];
//   const isAdmin = store.getters["user/isAdmin"];

//   const requiresAdmin = to.matched.some((record) => record.meta.requiresAdmin);

//   // If the route requires admin access and the user is not an admin, redirect
//   if (requiresAdmin && (!isAuthenticated || !isAdmin)) {
//     next({ name: isAdmin ? "Home" : "Login" });
//   } else if (
//     (to.name === "Login" || to.name === "Register") &&
//     isAuthenticated
//   ) {
//     // If the user is already authenticated, redirect to home
//     next({ name: "Home" });
//   } else {
//     next();
//   }
// });
export default router;
