import Axios from "axios";
import store from "@/store/index.js"; // Replace with the actual path to your Vuex store

const axios = Axios.create({
  baseURL: "http://localhost:8000",
  withCredentials: true,
  withXSRFToken: true,
});

axios.interceptors.response.use(null, (error) => {
  if (error.response) {
    const backendErrors = error.response.data.errors;
    store.dispatch("error/handleErrors", backendErrors, { root: true });
  }
  return Promise.reject(error);
});

export default axios;
