import axios from "@/lib/axios.js";

export default {
  namespaced: true,
  state: {
    authUser: null,
  },
  getters: {
    user: (state) => state.authUser,
    isAuthenticated: (state) => !!state.authUser,
    isAdmin: (state) => !!state.authUser && state.authUser.role === "admin",
  },
  mutations: {
    SET_USER(state, user) {
      state.authUser = user;
    },
    CLEAR_USER(state) {
      state.authUser = null;
    },
  },
  actions: {
    async login({ dispatch }, { email, password }) {
      await dispatch("getToken");
      try {
        await axios.post("/login", { email, password });

        await dispatch("getUser");
      } catch (error) {
        console.error(error.message);
      }
    },
    async register({ dispatch }, values) {
      await dispatch("getToken");
      try {
        await axios.post("/register", values);
        await dispatch("getUser");
      } catch (error) {
        console.error(error.message);
      }
    },
    async getUser({ dispatch, commit }) {
      await dispatch("getToken");
      try {
        const response = await axios.get("/api/user");
        commit("SET_USER", response.data);
      } catch (error) {
        if (error.response && error.response.status === 401) {
          // User is not authenticated, clear user state
          commit("CLEAR_USER");
        } else {
          // Handle other errors if needed
          console.error(error.message);
        }
      }
    },
    /**
     * Logs out the user.
     *
     * @param {object} dispatch - The dispatch function from the Vuex store.
     * @param {object} commit - The commit function from the Vuex store.
     * @return {Promise} A promise that resolves when the logout is successful and rejects if there is an error.
     */
    async logout({ dispatch, commit }) {
      await dispatch("getToken");
      try {
        await axios.post("/logout");
        commit("CLEAR_USER");
      } catch (error) {
        console.error(error.message);
      }
    },
    async forgotPassword({ dispatch }, { email }) {
      await dispatch("getToken");
      try {
        await axios.post("/forgot-password", { email });
      } catch (error) {
        console.error(error.message);
      }
    },
    async getToken() {
      try {
        await axios.get("/sanctum/csrf-cookie");
      } catch (error) {
        console.error(error.message);
      }
    },
  },
  modules: {},
};
