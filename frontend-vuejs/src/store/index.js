import { createStore } from "vuex";
import productModule from "./product/product.js";
import authModule from "./auth/auth.js";
import cartModule from "./order/cart.js";
import orderModule from "./order/order.js";
import customerModule from "./customer/customer.js";
import notificationsModule from "./notification/notifications.js";
import errorModule from "./error/error.js";

export default createStore({
  modules: {
    product: productModule,
    user: authModule,
    cart: cartModule,
    order: orderModule,
    customer: customerModule,
    notification: notificationsModule,
    error: errorModule,
  },
});
