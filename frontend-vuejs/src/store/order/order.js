import axios from "@/lib/axios.js";

export default {
  namespaced: true,
  state: {
    orders: [],
  },
  getters: {
    all: (state) => state.orders,
  },
  mutations: {
    SET_ORDERS(state, orders) {
      state.orders = orders;
    },
  },
  actions: {
    async sendOrder({ commit }, order) {
      try {
        await axios.post("/api/order", order);
        commit("cart/clearCart", null, { root: true });
      } catch (error) {
        console.error(error.message);
      }
    },
    async fetchAllOrders({ commit }) {
      try {
        const response = await axios.get("/api/orders");
        commit("SET_ORDERS", response.data.orders);
      } catch (error) {
        console.error(error.message);
      }
    },
  },
  modules: {},
};
