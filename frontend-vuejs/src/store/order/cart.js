export default {
  namespaced: true,
  state: {
    cart: [],
  },
  getters: {
    products: (state) => state.cart,
    total: (state) => {
      return state.cart.reduce((total, product) => {
        return total + Number(product.product.price) * product.quantity;
      }, 0);
    },
  },
  mutations: {
    addProductToCart(state, product) {
      state.cart.push(product);
    },
    removeProductFromCart(state, product) {
      state.cart = state.cart.filter((p) => p !== product);
    },
    clearCart(state) {
      state.cart = [];
    },
  },
  actions: {
    addProductToCart({ commit }, product) {
      commit("addProductToCart", product);
    },
    removeProductFromCart({ commit }, product) {
      commit("removeProductFromCart", product);
    },
  },
  modules: {},
};
