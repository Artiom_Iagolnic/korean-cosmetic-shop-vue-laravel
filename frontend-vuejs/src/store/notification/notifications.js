export default {
  namespaced: true,
  state: {
    notifications: [],
  },
  getters: {
    all: (state) => state.notifications,
  },
  mutations: {
    ADD_NOTIFICATION(state, notification) {
      state.notifications.push(notification);
    },
    REMOVE_NOTIFICATION(state, index) {
      state.notifications.splice(index, 1);
    },
  },
  actions: {
    addNotification({ commit }, notification) {
      commit("ADD_NOTIFICATION", notification);
      setTimeout(() => {
        commit("REMOVE_NOTIFICATION", 0); // Remove the notification after a timeout
      }, 2000); // Adjust the timeout duration as needed (e.g., 2000 milliseconds)
    },
  },
  modules: {},
};
