import axios from "@/lib/axios.js";

export default {
  namespaced: true,
  state: {
    customers: [],
  },
  getters: {
    all: (state) => state.customers,
    one: (state) => (id) =>
      state.customers.find((customer) => customer.id === id),
  },
  mutations: {
    SET_CUSTOMERS(state, customers) {
      state.customers = customers;
    },
  },
  actions: {
    async fetchAllCustomers({ dispatch, commit }) {
      try {
        const response = await axios.get("/api/customers");

        commit("SET_CUSTOMERS", response.data.customers);
      } catch (error) {
        dispatch(
          "notification/addNotification",
          {
            type: "error",
            title: "Error",
            text: `${error.message}`,
          },
          { root: true }
        );
      }
    },
    async deleteCustomer({ dispatch }, customer) {
      try {
        if (!customer) {
          dispatch(
            "notification/addNotification",
            {
              type: "error",
              title: "Error",
              text: "Customer not found",
            },
            { root: true }
          );
        }

        await axios.delete(`/api/customers/${customer.id}`);
      } catch (error) {
        dispatch(
          "notification/addNotification",
          {
            type: "error",
            title: "Error",
            text: `${error.message}`,
          },
          { root: true }
        );
      }
    },
  },
  modules: {},
};
