import axios from "@/lib/axios.js";

export default {
  namespaced: true,
  state: {
    products: [],
  },
  getters: {
    all: (state) => state.products,
    one: (state) => (slug) =>
      state.products.find((product) => product.slug === slug),
  },
  mutations: {
    SET_PRODUCTS(state, products) {
      state.products = products;
    },
  },
  actions: {
    async fetchProducts({ dispatch, commit }) {
      try {
        const response = await axios.get("/api/products");

        commit("SET_PRODUCTS", response.data.products);
      } catch (error) {
        dispatch(
          "notification/addNotification",
          {
            type: "error",
            title: "Error",
            text: `${error.message}`,
          },
          { root: true }
        );
      }
    },
    async addNew({ dispatch }, product) {
      try {
        if (!product) {
          console.log("there was no data added");
        }
        await axios.post("http://localhost:8000/api/products", product);
        dispatch("fetchProducts");
        dispatch(
          "notification/addNotification",
          {
            type: "success",
            title: "Success",
            text: "Product added successfully !",
          },
          { root: true }
        );
      } catch (error) {
        dispatch(
          "notification/addNotification",
          {
            type: "error",
            title: "Error",
            text: `${error.message}`,
          },
          { root: true }
        );
        console.error(error.message);
      }
    },
    async editProduct({ dispatch }, editedProduct) {
      try {
        if (!editedProduct) {
          console.log("error by editing a product");
          return;
        }
        await axios.put(
          `http://localhost:8000/api/products/${editedProduct.id}`,
          editedProduct
        );
        await dispatch("fetchProducts");
        dispatch(
          "notification/addNotification",
          {
            type: "success",
            title: "Success",
            text: "Product updated successfully.",
          },
          { root: true }
        );
      } catch (error) {
        dispatch(
          "notification/addNotification",
          {
            type: "error",
            title: "Error",
            text: `${error.message}`,
          },
          { root: true }
        );
        console.error(error.message);
      }
    },
    async deleteProduct({ dispatch }, product) {
      try {
        if (!product) {
          console.log("ther was no product with this id");
          return;
        }
        await axios.delete(`http://localhost:8000/api/products/${product.id}`);
        dispatch("fetchProducts");
        dispatch(
          "notification/addNotification",
          {
            type: "success",
            title: "Success",
            text: "Product deleted successfully !",
          },
          { root: true }
        );
      } catch (error) {
        dispatch(
          "notification/addNotification",
          {
            type: "error",
            title: "Error",
            text: `${error.message}`,
          },
          { root: true }
        );
        console.error(error.message);
      }
    },
  },
  modules: {},
};
