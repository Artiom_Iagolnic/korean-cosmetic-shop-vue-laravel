export default {
  namespaced: true,
  state: {
    errors: null,
  },
  getters: {
    all: (state) => state.errors,
  },
  mutations: {
    SET_ERRORS(state, error) {
      state.errors = error;
    },
    CLEAR_ERRORS(state) {
      state.errors = null;
    },
  },
  actions: {
    handleErrors({ commit }, error) {
      commit("SET_ERRORS", error);
    },
    clearError({ commit }) {
      commit("CLEAR_ERRORS");
    },
  },
};
